@extends('layouts.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Dashboard</div>

    <div class="panel-body">
        
        <div class="col-lg-4">
            <div class="panel panel-info">
                <div class="panel-heading text-center">
                    <h3 class="panel-title">Published Post</h3>
                </div>
                <div class="panel-body text-center">
                    {{$postsCount}}
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel panel-danger">
                <div class="panel-heading text-center">
                    <h3 class="panel-title">Trashed Post</h3>
                </div>
                <div class="panel-body text-center">
                    {{$trashCount}}
                </div>
            </div>
        </div>
        @if(Auth::user()->admin)
        <div class="col-lg-4">
            <div class="panel panel-success">
                <div class="panel-heading text-center">
                    <h3 class="panel-title">Users</h3>
                </div>
                <div class="panel-body text-center">
                    {{$userCount}}
                </div>
            </div>
        </div>
        @endif
        <div class="col-lg-4">
            <div class="panel panel-info">
                <div class="panel-heading text-center">
                    <h3 class="panel-title">Categories</h3>
                </div>
                <div class="panel-body text-center">
                    {{$categoryCount}}
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel panel-info">
                <div class="panel-heading text-center">
                    <h3 class="panel-title">Tags</h3>
                </div>
                <div class="panel-body text-center">
                    {{$tagCount}}
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection

@extends("layouts.app")

@section("content")

    @include("includes.errors")

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Edit Blog Setting</h3>
        </div>
        <div class="panel-body">

            <form action="{{route('setting.update')}}" method="post"  >
                {{csrf_field()}}
                <div class="form-group">
                    <label for="site_name">Site Name:</label>
                    <input type="text" name="site_name" value="{{$setting->site_name}}" id="site_name" class="form-control">
                </div>

                <div class="form-group">
                    <label for="contact_email">Contact Email:</label>
                    <input type="email" name="contact_email" value="{{$setting->contact_email}}" id="contact_email" class="form-control">
                </div>

                <div class="form-group">
                    <label for="contact_number">Contact Number:</label>
                    <input type="text" name="contact_number" value="{{$setting->contact_number}}" id="contact_number" class="form-control">
                </div>

                <div class="form-group">
                    <label for="address">Address:</label>
                    <textarea name="address"  id="address" cols="5" rows="3" class="form-control">{{$setting->address}}</textarea>
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>

            </form>
        </div>
    </div>
@endsection
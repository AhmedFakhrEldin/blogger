@extends("layouts.app")

@section("content")

    @include("includes.errors")

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Create New Post</h3>
        </div>
        <div class="panel-body">

            <form action="{{route('post.store')}}" method="post" enctype="multipart/form-data" >
                {{csrf_field()}}

                <div class="form-group">
                    <label for="category_id">Category Name</label>
                    <select name="category_id" id="category_id" class="form-control">
                        <option value=""> -- Select One -- </option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}"
                            @if(old('category_id')==$category->id)
                                 selected
                            @endif
                            >{{$category->name_category}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" value="{{old('title')}}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="title">Featured image</label>
                    <input type="file" name="featured" id="featured" class="form-control">
                </div>

                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea name="content" id="content" cols="5" rows="3" class="form-control">{{old('content')}}</textarea>
                </div>

                <div class="form-group">
                    <label for="tags">Select Tags</label>
                    <div class="pre-scrollable">
                        @foreach($tags as $tag)
                            <div class="checkbox">
                                <label>

                                    <input type="checkbox" value="{{$tag->id}}" name="tags[]"

                                    @if(is_array(old('tags')) && in_array($tag->id,old('tags')))
                                        checked
                                    @endif

                                    >
                                    {{$tag->tag}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-default">Submit</button>
                 </div>

            </form>
        </div>
    </div>
@endsection

@section('stylesPut')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('scriptPut')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#content').summernote();
        });
    </script>
@endsection
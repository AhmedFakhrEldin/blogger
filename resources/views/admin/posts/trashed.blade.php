@extends("layouts.app")

@section("content")

    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Category</th>
                    <th>Title</th>
                    <th colspan="2" width="15%">Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td><img src="{{asset($post->featured)}}" width="50" height="50"></td>
                            <td>{{$post->category->name_category}}</td>
                            <td>{{$post->title}}</td>
                            <td>
                                <a href="{{route('post.restore',['id'=>$post->id])}}" class="btn btn-xs btn-success"><i class="fa fa-window-restore"></i> Restore</a>
                            </td>
                            <td>
                                <form action="{{route("post.kill")}}" method="post" role="form" class="form-inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" id="id" value="{{$post->id}}">
                                    <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                                </form>
                            </td>
                        </tr>                        
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@extends("layouts.app")

@section("content")

    @include("includes.errors")

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Create New Category</h3>
        </div>
        <div class="panel-body">

            <form action="{{route('category.store')}}" method="post"  >
                {{csrf_field()}}

                <div class="form-group">
                    <label for="name_category">Category Name:</label>
                    <input type="text" name="name_category" id="name_category" class="form-control">
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>

            </form>
        </div>
    </div>
@endsection
@extends("layouts.app")

@section("content")


    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Category Name</th>
                    <th colspan="2" width="30%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->id}}</td>
                        <td>{{$category->name_category}}</td>
                        <td>
                            <a href="{{route('category.edit',['id' => $category->id])}}" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i> Update</a>
                        </td>
                        <td>
                            @if(Auth::user()->admin)
                            <form action="{{route("category.destroy",['id' => $category->id])}}" method="post" role="form" class="form-inline">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                            </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="panel-footer text-center ">
            {!! $categories->links() !!}
        </div>
    </div>
@endsection

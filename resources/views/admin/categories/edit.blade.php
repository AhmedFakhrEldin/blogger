@extends("layouts.app")

@section("content")

    @include("includes.errors")

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Edit Category</h3>
        </div>
        <div class="panel-body">

            <form action="{{route('category.update',['id'=>$category->id])}}" method="post"  >
                {{csrf_field()}}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label for="name_category">Category Name:</label>
                    <input type="text" name="name_category" value="{{$category->name_category}}" id="name_category" class="form-control">
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>

            </form>
        </div>
    </div>
@endsection
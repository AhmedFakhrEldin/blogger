@extends("layouts.app")

@section("content")

    @include("includes.errors")

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Edit Profile</h3>
        </div>
        <div class="panel-body">

            <form action="{{route('profile.update',['id'=>$user->id])}}" method="post" enctype="multipart/form-data" >
                {{csrf_field()}}
                {{method_field('PUT')}}
                <div class="form-group">
                    <label for="name">User Name</label>
                    <input type="text" name="name" id="name" value="{{$user->name}}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" value="{{$user->email}}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="password">Change Password</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>

                <div class="form-group">
                    <label for="about">About Me</label>
                    <textarea name="about" id="about" cols="5" rows="3" class="form-control">{{$user->profile->about}}</textarea>
                </div>

                <div class="form-group">
                    <label for="facebook">Facebook</label>
                    <input type="text" name="facebook" id="facebook" value="{{$user->profile->facebook}}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="youtube">YouTube</label>
                    <input type="text" name="youtube" id="youtube" value="{{$user->profile->youtube}}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="avatar">Avatar image</label>
                    <input type="file" name="avatar" id="avatar" class="form-control">
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>

            </form>
        </div>
    </div>
@endsection
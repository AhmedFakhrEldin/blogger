@extends("layouts.app")

@section("content")

    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Permissions</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td><img src="{{(!empty($user->profile->avatar))? asset($user->profile->avatar):asset('img/defaultimage.png')}}" width="50" height="50" style="border-radius: 50%"></td>
                        <td>{{$user->name}}</td>
                        <td>{{($user->admin)? 'Admin':'User'}}</td>
                        <td>
                            @if($user->id!=Auth::user()->id)
                            <form action="{{route("user.destroy",['id'=>$user->id])}}" method="post" role="form" class="form-inline">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                            </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="panel-footer text-center ">
            {!! $users->links() !!}
        </div>
    </div>
@endsection

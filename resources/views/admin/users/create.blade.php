@extends("layouts.app")

@section("content")

    @include("includes.errors")
    @include("includes.flash")

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Create User</h3>
        </div>
        <div class="panel-body">

            <form action="{{route('user.store')}}" method="post" enctype="multipart/form-data" >
                {{csrf_field()}}

                <div class="form-group">
                    <label for="name">User Name</label>
                    <input type="text" name="name" id="name" class="form-control">
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control">
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>

            </form>
        </div>
    </div>
@endsection
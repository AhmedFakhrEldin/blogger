@extends("layouts.app")

@section("content")

    @include("includes.errors")

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Edit Tag</h3>
        </div>
        <div class="panel-body">

            <form action="{{route('tag.update',['id'=>$tag->id])}}" method="post"  >
                {{csrf_field()}}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label for="tag">tag Name:</label>
                    <input type="text" name="tag" value="{{$tag->tag}}" id="tag" class="form-control">
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>

            </form>
        </div>
    </div>
@endsection
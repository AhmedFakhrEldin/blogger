@extends("layouts.app")

@section("content")

    @include("includes.errors")

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Create New Tag</h3>
        </div>
        <div class="panel-body">

            <form action="{{route('tag.store')}}" method="post"  >
                {{csrf_field()}}

                <div class="form-group">
                    <label for="tag">Tag Name:</label>
                    <input type="text" name="tag" id="tag" class="form-control">
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>

            </form>
        </div>
    </div>
@endsection
@extends('layouts.default')

@section('headerPages')
    <div class="stunning-header stunning-header-bg-lightviolet">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">{{$filter}}</h1>
        </div>
    </div>
@endsection

@section('content')
<div class="row medium-padding120">
    <main class="main">

        <div class="row">
            @if(count($filterData)>0)
                <div class="case-item-wrap">
                    @foreach($filterData as $post)
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="case-item">
                                <div class="case-item__thumb">
                                    <img src="{{asset($post->featured)}}" alt="{{$post->title}}">
                                </div>
                                <h6 class="case-item__title"><a href="{{route('post.view',['slug'=>$post->slug])}}">{{$post->title}}</a></h6>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <h1 class="text-center">
                    No Result Found.
                </h1>
            @endif
        </div>

        <!-- End Post Details -->

        <!-- Sidebar-->
        @include('elements.blogstags')
        <!-- End Sidebar-->

    </main>
</div>
@endsection
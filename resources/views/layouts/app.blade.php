<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{asset("app/css/fonts.css")}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/toastr.min.css') }}"  rel="stylesheet">
    <link href="{{ asset('css/costum.css') }}"  rel="stylesheet">
    @yield('stylesPut')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{route('profile.index')}}">My Account</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="list-group">
                        <a href="\admin" class="list-group-item">Dashboard</a>
                        @if(Auth::user()->admin)
                            <a class="list-group-item" href="#sub_userPart" id="userPart" data-toggle="collapse" data-parent="#sub_userPart">
                                <span>Users</span>
                                <span class="menu-ico-collapse pull-right"><i class="fa fa-chevron-down"></i></span>
                            </a>
                            <div class="collapse list-group-submenu" id="sub_userPart">
                                <a href="{{route('user.index')}}" class="list-group-item sub-item">Users List</a>
                                <a href="{{route('user.create')}}" class="list-group-item sub-item">New User</a>
                            </div>
                        @endif
                        <a class="list-group-item" href="#sub_post" id="postPart" data-toggle="collapse" data-parent="#sub_post">
                            <span>Posts</span>
                            <span class="menu-ico-collapse pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                        <div class="collapse list-group-submenu" id="sub_post">
                            <a href="{{route('post.index')}}" class="list-group-item sub-item">Posts List</a>
                            <a href="{{route('post.create')}}" class="list-group-item sub-item">Create New Post</a>
                            <a href="{{route('post.trashed')}}" class="list-group-item sub-item">Posts Trashed List</a>
                        </div>
                        <a class="list-group-item" href="#sub_category" id="categoryPart" data-toggle="collapse" data-parent="#sub_category">
                            <span>Categories</span>
                            <span class="menu-ico-collapse pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                        <div class="collapse list-group-submenu" id="sub_category">
                            <a href="{{route('category.index')}}" class="list-group-item sub-item">Categories List</a>
                            <a href="{{route('category.create')}}" class="list-group-item sub-item">Create New Category</a>
                        </div>
                        <a class="list-group-item" href="#sub_tag" id="tagPart" data-toggle="collapse" data-parent="#sub_tag">
                            <span>Tags</span>
                            <span class="menu-ico-collapse pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                        <div class="collapse list-group-submenu" id="sub_tag">
                            <a href="{{route('tag.index')}}" class="list-group-item sub-item">Tags list</a>
                            <a href="{{route('tag.create')}}" class="list-group-item sub-item">Create New Tag</a>
                        </div>
                        @if(Auth::user()->admin)
                            <a href="{{route('setting')}}" class="list-group-item">Setting</a>
                        @endif


                    </div>
                </div>
                <div class="col-lg-8">
                    @yield('content')
                </div>
            </div>
        </div>



    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    @yield('scriptPut')
    @include("includes.toastaFlash")
</body>
</html>

@if(Session::has('Succeed'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Succeed!</strong> {{Session::get('Succeed')}}
    </div>
@elseif(Session::has('Error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> {{Session::get('Error')}}
    </div>
@endif
@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <article class="hentry post post-standard has-post-thumbnail sticky">

                <div class="post-thumb">
                    <img src="{{asset($firstPost->featured)}}" alt="{{$firstPost->title}}" height="388">
                    <div class="overlay"></div>
                    <a href="{{asset($firstPost->featured)}}" class="link-image js-zoom-image">
                        <i class="seoicon-zoom"></i>
                    </a>
                    <a href="{{route('post.view',['slug'=>$firstPost->slug])}}" class="link-post">
                        <i class="seoicon-link-bold"></i>
                    </a>
                </div>

                <div class="post__content">

                    <div class="post__content-info">

                        <h2 class="post__title entry-title ">
                            <a href="{{route('post.view',['slug'=>$firstPost->slug])}}">{{$firstPost->title}}</a>
                        </h2>

                        <div class="post-additional-info">

                                        <span class="post__date">

                                            <i class="seoicon-clock"></i>

                                            <time class="published" datetime="{{$firstPost->created_at}}">
                                                {{$firstPost->created_at->diffForHumans()}}
                                            </time>

                                        </span>

                            <span class="category">
                                            <i class="seoicon-tags"></i>
                                            <a href="{{route('category.filter',['id'=>$firstPost->category_id])}}">{{$firstPost->category->name_category}}</a>
                                        </span>

                            <span class="post__comments">
                                            <a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i></a>
                                            6
                                        </span>

                        </div>
                    </div>
                </div>

            </article>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <div class="row">
        @foreach($sndThPosts as $sndThPost)
        <div class="col-lg-6">
            <article class="hentry post post-standard has-post-thumbnail sticky">

                <div class="post-thumb">
                    <img src="{{asset($sndThPost->featured)}}" alt="{{$sndThPost->title}}" height="280">
                    <div class="overlay"></div>
                    <a href="{{asset($sndThPost->featured)}}" class="link-image js-zoom-image">
                        <i class="seoicon-zoom"></i>
                    </a>
                    <a href="{{route('post.view',['slug'=>$sndThPost->slug])}}" class="link-post">
                        <i class="seoicon-link-bold"></i>
                    </a>
                </div>

                <div class="post__content">

                    <div class="post__content-info">

                        <h2 class="post__title entry-title ">
                            <a href="{{route('post.view',['slug'=>$sndThPost->slug])}}">{{$sndThPost->title}}</a>
                        </h2>

                        <div class="post-additional-info">

                                        <span class="post__date">

                                            <i class="seoicon-clock"></i>

                                            <time class="published" datetime="{{$sndThPost->created_at}}">
                                                {{$sndThPost->created_at->toFormattedDateString()}}
                                            </time>

                                        </span>

                            <span class="category">
                                            <i class="seoicon-tags"></i>
                                            <a href="{{route('category.filter',['id'=>$sndThPost->category_id])}}">{{$sndThPost->category->name_category}}</a>
                                        </span>

                            <span class="post__comments">
                                            <a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i></a>
                                            6
                                        </span>

                        </div>
                    </div>
                </div>

            </article>
        </div>
        @endforeach
    </div>
@endsection

@section('homContain')
    <div class="container-fluid">
        <div class="row medium-padding120 bg-border-color">
            <div class="container">
                <div class="col-lg-12">
                    <div class="offers">
                        <div class="row">
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <h4 class="h1 heading-title">{{$news->name_category}}</h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="case-item-wrap">
                            @foreach($news->posts()->orderBy('created_at','desc')->take(3)->get() as $post)
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="case-item">
                                        <div class="case-item__thumb">
                                            <img src="{{asset($post->featured)}}" alt="{{$post->title}}" height="388">
                                        </div>
                                        <h6 class="case-item__title"><a href="{{route('post.view',['slug'=>$post->slug])}}">{{$post->title}}</a></h6>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="padded-50"></div>
                    <div class="offers">
                        <div class="row">
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <h4 class="h1 heading-title">{{$technology->name_category}}</h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="case-item-wrap">
                                @foreach($technology->posts()->orderBy('created_at','desc')->take(3)->get() as $post)
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="case-item">
                                            <div class="case-item__thumb">
                                                <img src="{{asset($post->featured)}}" alt="{{$post->title}}" height="388">
                                            </div>
                                            <h6 class="case-item__title"><a href="{{route('post.view',['slug'=>$post->slug])}}">{{$post->title}}</a></h6>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="padded-50"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
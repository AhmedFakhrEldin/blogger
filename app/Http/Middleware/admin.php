<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->admin){
            Session::flash('Info','You are not allowed to access this page');
            return redirect()->back();
        }
        return $next($request);
    }
}

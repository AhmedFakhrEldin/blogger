<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use App\User;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->admin){
            return view('admin.home')
                ->with('postsCount',Post::where('user_id',Auth::id())->count())
                ->with('trashCount',Post::onlyTrashed()->where('user_id',Auth::id())->count())
                ->with('categoryCount',Category::all()->count())
                ->with('tagCount',Tag::all()->count());
        }else{
            return view('admin.home')
                ->with('postsCount',Post::all()->count())
                ->with('trashCount',Post::onlyTrashed()->get()->count())
                ->with('userCount',User::all()->count())
                ->with('categoryCount',Category::all()->count())
                ->with('tagCount',Tag::all()->count());

        }

    }
}

<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Session;

class SettingsControllser extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }


    public function index(){
        return view('admin.settings.setting')->with('setting',Setting::first());
    }

    public function update(){

        $this->validate(request(),[
            'site_name' => 'required',
            'contact_email' => 'required|email',
            'contact_number' => 'required',
            'address' => 'required'
        ]);

        $settings = Setting::first();
        $settings->site_name = request()->site_name;
        $settings->contact_email = request()->contact_email;
        $settings->contact_number = request()->contact_number;
        $settings->address = request()->address;

        if($settings->save()){
            Session::flash('Succeed',"Saved Successfully");
        }else{
            Session::flash('Error',"Operation Not Done");
        }

        return redirect()->route('setting');
    }
}

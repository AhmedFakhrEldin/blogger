<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Auth;
use Illuminate\Http\Request;
use Session;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->admin){
            $posts = Post::paginate(10);
        }else{
            $posts = Post::where('user_id',Auth::id())->paginate(10);
        }
        return view('admin.posts.index')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        $tags = Tag::all();
        if($categories->count()==0 || $tags->count()==0){

            Session::flash('Info','You must have some category or tags to pull post');
            return redirect()->back();
        }

        return view('admin.posts.create')
            ->with('categories',$categories)
            ->with('tags',$tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'title'=>'required|max:255',
            'featured' => 'required|image',
            'content' => 'required',
            'category_id' => 'required',
            'tags' => 'required'
        ]);

        $featured = $request->file('featured');
        $featured_new_name = time().$request->file('featured')->getClientOriginalName();
        $featured->move('uploads/posts',$featured_new_name);

        /*$post = new Post();

        $post->title = $request->input('title');
        $post->featured = $featured_new_name;
        $post->content = $request->input('content');
        $post->category_id = $request->input('category_id');*/

       $post = Post::create([
           'title' => $request->input('title'),
           'featured' => 'uploads/posts/'.$featured_new_name,
           'content' => $request->input('content'),
           'category_id' => $request->input('category_id'),
           'slug' => str_slug($request->input('title')),
           'user_id' => Auth::id()
       ]);


        if($post->exists){
            $post->tags()->attach($request->tags);
            Session::flash('Succeed',"Saved Successfully");
        }else{
            Session::flash('Error',"Operation Not Done");
        }

        return redirect()->route('post.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(Auth::user()->admin){
            $post = Post::find($id);
        }else{
            $post = Post::where(['user_id'=>Auth::id(),'id'=>$id])->first();
        }

        return view("admin.posts.edit")
            ->with('post',$post)
            ->with('categories',Category::all())
            ->with('tags',Tag::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'title'=>'required|max:255',
            'content' => 'required',
            'category_id' => 'required'
        ]);

        $post = Post::find($id);

        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->category_id = $request->input('category_id');

        if($request->hasFile('featured')){
            $featured = $request->file('featured');
            $featured_new_name = time().$request->file('featured')->getClientOriginalName();
            $featured->move('uploads/posts',$featured_new_name);
            $post->featured = 'uploads/posts/'.$featured_new_name;
        }

        if($post->save()){
            $post->tags()->sync($request->tags);
            Session::flash('Succeed',"Saved Successfully");
        }else{
            Session::flash('Error',"Operation Not Done");
        }

        return redirect()->route('post.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(Auth::user()->admin){
            $post = Post::find($id);
        }else{
            $post = Post::where(['user_id'=>Auth::id(),'id'=>$id])->first();
        }

        if($post->delete()){
            Session::flash('Succeed',"Trashed Successfully");
        }else{
            Session::flash('Error',"Operation Not Done");
        }

        return redirect()->route('post.index');
    }

    public function trashed(){

        if(Auth::user()->admin){
            $posts = Post::onlyTrashed()->get();
        }else{
            $posts = Post::onlyTrashed()->where('user_id',Auth::id())->get();
        }

        return view('admin.posts.trashed')->with('posts',$posts);
    }

    public function kill(Request $request){

        $post = Post::withTrashed()->where('id',$request->id)->first();

        if($post->forceDelete()){
            Session::flash('Succeed',"Deleted Successfully");
        }else{
            Session::flash('Error',"Operation Not Done");
        }

        return redirect()->route('post.index');
    }

    public function restore($id){

        if(Auth::user()->admin){
            $post = Post::withTrashed()->where('id',$id)->first();
        }else{
            $post = Post::withTrashed()->where(['id'=>$id,'user_id'=>Auth::id()])->first();
        }

        if($post->restore()){
            Session::flash('Succeed',"Restored Successfully");
        }else{
            Session::flash('Error',"Operation Not Done");
        }

        return redirect()->route('post.index');
    }
}

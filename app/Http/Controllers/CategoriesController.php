<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return view("admin.categories.index")->with('categories',Category::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name_category' => 'required'
        ]);

        $category = new Category();
        $category->name_category =  $request->name_category;
        $category->slug =  str_slug($request->name_category);
        if($category->save()){
            Session::flash('Succeed',"Saved Successfully");
        }else{
            Session::flash('Error',"Operation Not Done");
        }

        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = Category::find($id);

        return view("admin.categories.edit")->with('category',$category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'name_category' => 'required'
        ]);

        $category=Category::find($id);
        $category->name_category = $request->name_category;
        $category->slug =  str_slug($request->name_category);
        if($category->save()){
            Session::flash('Succeed',"Updated Successfully");
        }else{
            Session::flash('Error',"Operation Not Done");
        }

        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category=Category::find($id);

        foreach ($category->posts as $post){
            //$post->delete();
            $post->forceDelete();
        }

        if($category->Delete()){
            Session::flash('Succeed',"Deleted Successfully");
        }else{
            Session::flash('Error',"Operation Not Done");
        }

        return redirect()->route('category.index');
    }
}

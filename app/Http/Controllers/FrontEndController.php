<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Setting;
use App\Tag;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    //

    public function index(){

        $viewSite = true;
        if(Setting::all()->count()==0){
            $viewSite=false;
        }

        if(Category::all()->count()<7){
            $viewSite=false;
        }

        if (!$viewSite){
            return redirect('\admin');
        }


        return view('index')
            ->with('title',Setting::first()->site_name)
            ->with('categories',Category::take(5)->get())
            ->with('firstPost',Post::whereNotIn('category_id',[6,7])->orderBy('created_at','desc')->first())
            ->with('sndThPosts',Post::whereNotIn('category_id',[6,7])->orderBy('created_at','desc')->skip(1)->take(2)->get())
            ->with('news',Category::find(6))
            ->with('technology',Category::find(7))
            ->with('setting',Setting::first());
    }

    public function article($slug){

        $post = Post::where('slug','=', $slug)->first();

        $postPrevId = Post::where('id','<', $post->id)->max('id');
        $postNextId = Post::where('id','>', $post->id)->min('id');

        return view('single')
            ->with('title',$post->title)
            ->with('categories',Category::take(5)->get())
            ->with('setting',Setting::first())
            ->with('post',$post)
            ->with('postPrev',Post::find($postPrevId))
            ->with('postNext',Post::find($postNextId))
            ->with('tags',Tag::all());
    }

    public function category($id){

        $filterCat = Category::find($id);
        return $this->filterView($filterCat,$filterCat->name_category,'Category: ');

    }

    public function view($slug){
        $filterCat = Category::where('slug','=', $slug)->first();
        return $this->filterView($filterCat,$filterCat->name_category);
    }

    public function tag($id){
        $filterTag = Tag::find($id);
        return $this->filterView($filterTag,$filterTag->tag,'Tag: ');
    }


    private function filterView($filterAs,$name,$text=null){
        return view('filter')
            ->with('title',$name)
            ->with('filter',$text.$name)
            ->with('filterData',$filterAs)
            ->with('categories',Category::take(5)->get())
            ->with('setting',Setting::first())
            ->with('tags',Tag::all());
    }
}

<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = App\User::create([
            'name' => 'Ahmed Mahmoud',
            'email'=> 'startprog82@gmail.com',
            'password' => bcrypt("123456"),
            'admin' => 1
        ]);

        App\Profile::create([
           'user_id' => $user->id,
            'avatar' => 'img/defaultimage.png',
            'about' => 'dfa fdf f sf zsdf asgdg sgzf sg sfds gdg dfg',
            'facebook' => 'https://www.facebook.com',
            'youtube' => 'https://www.youtube.com'
        ]);
    }
}

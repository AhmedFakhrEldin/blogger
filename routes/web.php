<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Category;
use App\Post;
use App\Setting;
use App\Tag;
use Illuminate\Support\Facades\Session;
use Spatie\Newsletter\NewsletterFacade as Newsletter;


Route::get('/',[
    'uses' => 'FrontEndController@index',
    'as' => 'index'
]);

Route::get('/article/{slug}',[
    'uses' => 'FrontEndController@article',
    'as' => 'post.view'
]);

Route::get('/category/{id}',[
    'uses'=>'FrontEndController@category',
    'as'=>'category.filter'
])->where('id','[0-9]+');

Route::get('/category/{slug}',[
    'uses'=>'FrontEndController@view',
    'as'=>'category.view'
]);

Route::get('/tag/{id}',[
    'uses'=>'FrontEndController@tag',
    'as'=>'tag.posts'
])->where('id','[0-9]+');

Route::get('/result/{slug}',[
    'uses'=>'FrontEndController@view',
    'as'=>'category.view'
]);

Route::get('/search',function(){
    $posts = Post::where('title','like','%'.request('query').'%')->get();
    return view('result')
        ->with('title','Search')
        ->with('filter','Result: '.request('query'))
        ->with('filterData',$posts)
        ->with('categories',Category::take(5)->get())
        ->with('setting',Setting::first())
        ->with('tags',Tag::all());
});

Route::post('/newsletter',function(){

    $email = request('email');

    if(Newsletter::isSubscribed($email)){
        Session::flash('Info',"this email {{$email}} subscribe before");
    }else{
        if(Newsletter::subscribe($email)){
            Session::flash('Succeed','Thank you for subscribe');
        }else{
            Session::flash('Error','Fail to subscribe please try again latter');
        }
    }

    return redirect()->back();
});

Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){

    Route::get('/post/trashed',[
        'uses' => 'PostsController@trashed',
        'as' => 'post.trashed'
    ]);
    Route::get('/post/restore/{id}',[
        'uses' => 'PostsController@restore',
        'as' => 'post.restore'
    ]);
    Route::post('/post/kill',[
        'uses' => 'PostsController@kill',
        'as' => 'post.kill'
    ]);
    Route::resource("/category","CategoriesController",['except' => 'show']);
    Route::resource('/post','PostsController',['except' => 'show']);
    Route::resource('/tag','TagsController',['except' => 'show']);
    Route::resource('/user','UsersController',[
        'except' => ['show','edit','update']
    ]);
    Route::resource('/profile','ProfilesController');

    Route::get('/setting',[
        'uses' => 'SettingsControllser@index',
        'as' => 'setting'
    ]);

    Route::post('/setting/update',[
        'uses' => 'SettingsControllser@update',
        'as' => 'setting.update'
    ]);
});


